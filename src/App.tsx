import React from 'react';
import logo from './logo.svg';
import './App.css';
import styled from 'styled-components/macro';

const Title = styled.p`
  font-size: 2em;
  border-bottom: 2px solid lime;
`;

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <Title>styled!</Title>
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.tsx</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
      </header>
    </div>
  );
}

export default App;
